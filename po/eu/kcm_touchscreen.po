# Translation for kcm_touchscreen.po to Euskara/Basque (eu).
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-09 01:37+0000\n"
"PO-Revision-Date: 2022-11-24 05:50+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: kcmtouchscreen.cpp:46
#, kde-format
msgid "Automatic"
msgstr "Automatikoa"

#: kcmtouchscreen.cpp:52
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: ui/main.qml:28
#, kde-format
msgid "No touchscreens found"
msgstr "Ez da ukimen-pantailarik aurkitu"

#: ui/main.qml:44
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Gailua:"

#: ui/main.qml:57
#, kde-format
msgid "Enabled:"
msgstr "Gaituta:"

#: ui/main.qml:65
#, kde-format
msgid "Target display:"
msgstr "Helburuko pantaila:"
